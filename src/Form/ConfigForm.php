<?php

namespace Drupal\media_placeholder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 *
 * Setup config form.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_placeholder.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_placeholder.config');

    $form['provider'] = [
      '#type' => 'select',
      '#title' => 'Image replacement provider',
      '#desctiption' => $this->t('Select a provider for replacement images.'),
      '#options' => [
        'provider_placeholder' => $this->t('placeholder.com'),
        'provider_picsum' => $this->t('picsum.photos'),
      ],
      '#default_value' => ($config->get('provider')) ? $config->get('provider') : 'provider_picsum',
    ];

    $form['provider_picsum'] = [
      '#title' => $this->t('picsum.photos'),
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="provider"]' => ['value' => 'provider_picsum'],
        ],
      ],
    ];
    $form['provider_picsum']['info'] = [
      '#type' => 'item',
      '#markup' => $this->t('Service provided by <a href="https://picsum.photos/">picsum.photos</a>.<br /> We use <strong>Static Random Images</strong> with a "static" seed based on image config variables.'),
    ];

    $form['provider_placeholder'] = [
      '#title' => $this->t('placeholder.com'),
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="provider"]' => ['value' => 'provider_placeholder'],
        ],
      ],
    ];
    $form['provider_placeholder']['info'] = [
      '#type' => 'item',
      '#markup' => $this->t('Service provided by <a href="https://placeholder.com/">placeholder.com</a>'),
    ];
    $form['provider_placeholder']['img_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image text'),
      '#default_value' => ($config->get('img_text')) ? $config->get('img_text') : 'Image not found!',
      '#description' => $this->t('Text to show in an image'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('media_placeholder.config')
      ->set('img_text', $form_state->getValue('img_text'))
      ->set('provider', $form_state->getValue('provider'))
      ->save();
  }

}
